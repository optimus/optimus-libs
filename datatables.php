<?php
function datatable_request($connection, $resource, $db, $table, $restrictions = [])
{
	global $input;
	
	if (isset($input->body->columns))
		columns_validation($input->body->columns, $resource);
	if (isset($input->body->sort))
		sort_validation($input->body->sort, $resource);
	if (isset($input->body->filter))
		filter_validation($input->body->filter, $resource);
	if (isset($input->body->size))
	{
		if ($input->body->size === true)
			$input->body->size = 9999999999;
		check('size', $input->body->size, 'positive_integer');
	}
	if (isset($input->body->page))
		check('page', $input->body->page, 'positive_integer');
	if (isset($input->body->references))
		check('references', $input->body->references, 'boolean');
	if ($input->body->references === false)
			$resource = (object)array_filter((array)$resource, function($column){return !$column->reference;});
	$results = datatable_query($connection, $resource, $db, $table, $restrictions);
	return $results;
}

function columns_validation($column_array, $resource)
{
	foreach ($column_array as $column)
		if (!in_array($column, array_keys((array)$resource)))
			output(array("code" => 400, "message" => "La colonne " . $column . " n'existe pas."));
}

function sort_validation($sort_array, $resource)
{
	foreach ($sort_array as $sort)
	{
		if (strpos($sort->field, '.'))
		{
			$sort->subfield = substr($sort->field, strpos($sort->field, '.') + 1);
			$sort->field = substr($sort->field, 0, strpos($sort->field, '.'));
		}

		if (!in_array($sort->field, array_keys((array)$resource)))
			output(array("code" => 400, "message" => "Tri sur la colonne " . $sort->field . " : cette colonne n'existe pas dans la base de données."));
		else if (strtolower($sort->dir) != 'desc' AND strtolower($sort->dir) != 'asc')
			output(array("code" => 400, "message" => "Tri sur la colonne " . $sort->field . " : la direction ne peut prendre que les valeurs 'asc' ou 'desc'."));
	}
}

function filter_validation($filter_array, $resource)
{
	foreach ($filter_array as $filter)
	{
		if (strpos($filter->field, '.'))
		{
			$filter->subfield = substr($filter->field, strpos($filter->field, '.') + 1);
			$filter->field = substr($filter->field, 0, strpos($filter->field, '.'));
		}

		if (is_array($filter))
			return filter_validation($filter, $resource);
		else if ($filter->field != '*' AND !in_array($filter->field, array_keys((array)$resource)))
			output(array("code" => 400, "message" => "Filtre sur la colonne " . $filter->field . " : cette colonne n'existe pas dans la base de données."));
		else if (!in_array($filter->type, ["=", "!=", "like", "keywords", "starts", "ends", "<", ">", "<=", ">=", "in", "regex", "isnull", "isnotnull"]))
			output(array("code" => 400, "message" => "Filtre sur la colonne " . $filter->field . " : l'opérateur spécifié est inconnu."));
		$filter->value = check($filter->field, $filter->value, $filter->field == '*' ? 'string' : $resource->{$filter->field}->type);
	}
}

function datatable_filter($query, $resource, $filter_array)
{
	$query = '(';
	foreach($filter_array as $filter)
		if ($filter->field != '*')
			if (is_array($filter))
			{
				$query = substr($query,0,-6);
				$query .= ') AND ' . datatable_filter($query, $resource, $filter) . ' AND (';
			}
			else
				$query .= datatable_comparator($resource, $filter) . '  OR  ';
	$query = substr($query,0,-6);
	if (!is_array($filter))
		$query .= ')';
	return $query;
}

function datatable_comparator($resource, $filter)
{
	if ($filter->subfield)
		$comparator = "JSON_VALUE(" . $resource->{$filter->field}->field . ", '$." . $filter->subfield . "') ";
	else
		$comparator = $resource->{$filter->field}->field . ' ';

	if (in_array(strtolower($filter->type), ["like", "starts", "ends"]))
		$comparator .= "LIKE :var" . crc32(json_encode($filter));
	else if (in_array(strtolower($filter->type), ["isnull"]))
		$comparator .= "IS NULL";
	else if (in_array(strtolower($filter->type), ["isnotnull"]))
		$comparator .= "IS NOT NULL";
	else
		$comparator .= $filter->type . " :var"  . crc32(json_encode($filter));
	return $comparator;
}

function datatable_bindparam($prepared_query, $filter_array, $resource)
{
	foreach($filter_array as $filter)
		if (is_array($filter))
			datatable_bindparam($prepared_query, $filter, $resource);
		else
		{
			if (stripos($resource->{$filter->field}->type, 'integer') !== false)
				$pdo_type = PDO::PARAM_INT;
			else
				$pdo_type = PDO::PARAM_STR;

			if (strtolower($filter->type) == 'like')
			{
				$value = '%'.$filter->value.'%';
				$prepared_query->bindValue(':var'.crc32(json_encode($filter)), $value, PDO::PARAM_STR);
			}
			else if (strtolower($filter->type) == 'starts')
			{
				$value = $filter->value.'%';
				$prepared_query->bindValue(':var'.crc32(json_encode($filter)), $value, PDO::PARAM_STR);
			}
			else if (strtolower($filter->type) == 'ends')
			{
				$value = '%'.$filter->value;
				$prepared_query->bindValue(':var'.crc32(json_encode($filter)), $value, PDO::PARAM_STR);
			}
			else if (strtolower($filter->type) != 'isnull' AND strtolower($filter->type) != 'isnotnull')
			{
				$value = $filter->value;
				$prepared_query->bindValue(':var'.crc32(json_encode($filter)), $value, $pdo_type);
			}
		}
}

function datatable_query($connection, $resource, $db, $table, $restrictions = [])
{
	global $input;

	$query = "SELECT SQL_CALC_FOUND_ROWS ";
	
	//FIELDS
	foreach (array_keys((array)$resource) as $key)
		if (!isset($input->body->columns) OR in_array($key, $input->body->columns))
			$query .= $resource->$key->field . ' as `' . $key . '`, ';
	$query = substr($query, 0, -2);

	$query .= " FROM `" . $db . "`.`" . $table . "` ";

	//LEFT JOIN
	foreach (array_keys((array)$resource) as $key)
		if ($resource->$key->reference)
			if (strpos($query, " LEFT JOIN " . $resource->$key->reference->db . ' ON ' . $resource->$key->reference->id . ' = ' . $resource->$key->reference->match) === false)
				$query .= " LEFT JOIN " . $resource->$key->reference->db . ' ON ' . $resource->$key->reference->id . ' = ' . $resource->$key->reference->match;

	$query .= " WHERE 1=1 ";

	//FILTERS
	if (isset($input->body->filter) AND sizeof($input->body->filter) > 0)
	{
		$query .= ' AND ';
		foreach($input->body->filter as $filter)
			if ($filter->field != '*')	
				if (is_array($filter))
					$query .= '(' . datatable_filter($query, $resource, $filter) . ')  AND ';
				else
					$query .= datatable_comparator($resource, $filter) . '  AND ';
		$query = substr($query,0,-6);
	}

	//GLOBAL_SEARCH
	if (isset($input->body->filter) AND sizeof($input->body->filter) > 0)
		foreach($input->body->filter as $filter)
			if ($filter->field == '*')	
			{
				$query .= ' AND (';
				foreach (array_keys((array)$resource) as $key)
					if (!isset($input->body->columns) OR in_array($key, $input->body->columns))
					{
						if (in_array($resource->$key->type, ['integer','positive_integer', 'negative_integer','strictly_positive_integer','strictly_negative_integer','decimal','boolean']) AND !is_numeric($filter->value))
							continue;
						else
							$query .= $resource->$key->field . datatable_comparator($resource, $filter) . " OR ";
						
					}
				$query = substr($query,0,-4) . ')';
				break;
			}

	//APPLY RESTRICTIONS
	if ($restrictions && sizeof($restrictions) > 0)
		if (in_array('read', $restrictions[$table]))
		{
			//GLOBAL READ RESTRICTION APPLIES - ONLY SHOW RESOURCES FOR WHICH AN EXPLICIT READ AUTHORIZATION EXISTS
			foreach ($restrictions as $key => $restriction)
				if (!in_array('read', $restriction))
					$inclusion_list[] = (int)explode('/', $key)[1];
			if (sizeof($inclusion_list) > 0)
				$query .= ' AND ' . $resource->id->field . ' IN (' . implode(', ', $inclusion_list) . ')';
		}
		else
		{
			//NO GLOBAL READ RESTRICTION APPLIES - HIDE RESOURCES FOR WHICH AN EXPLICIT READ RESTRICTION EXISTS
			foreach ($restrictions as $key => $restriction)
				if (in_array('read', $restriction))
					$exclusion_list[] = (int)explode('/', $key)[1];
			if (sizeof($exclusion_list) > 0)
				$query .= ' AND ' . $resource->id->field . ' NOT IN (' . implode(', ', $exclusion_list) . ')';
		}

	//ORDER
	if (isset($input->body->sort) AND sizeof($input->body->sort) > 0)
	{
		$query .= ' ORDER BY ';
		foreach($input->body->sort as $sort)
			if ($sort->subfield)
				$query .= "JSON_VALUE(" . $resource->{$sort->field}->field . ", '$." . $sort->subfield . "') " . (strtolower($sort->dir) == 'desc' ? ' DESC,' : ' ASC,');
			else
				$query .= $sort->field . (strtolower($sort->dir) == 'desc' ? ' DESC,' : ' ASC,');
		$query = substr($query,0,-1);
	}

	//LIMIT
	if (isset($input->body->size) AND $input->body->size > 0)
	{
		$query .= ' LIMIT ' . $input->body->size;
		if (isset($input->body->page))
			$query .= ' OFFSET ' . ($input->body->size * ($input->body->page-1));
	}
	
	//PREPARE QUERY
	$prepared_query = $connection->prepare($query);
	
	//BIND PARAM
	if (isset($input->body->filter))
		datatable_bindparam($prepared_query, $input->body->filter, $resource);
	
	//EXECUTE
	if($prepared_query->execute())
		return $prepared_query->fetchAll(PDO::FETCH_ASSOC);
	else
		output(["code" => 400, "data" => [], "total" => 0, "last_page" => 1, "message" => $prepared_query->errorInfo()[2]]);
}


function datatables_insert($connection, $resource, $db, $table)
{
	global $input;

	$raw_resource = (object)array_filter((array)$resource, function($column){return (!$column->reference AND !$column->virtual);});
	
	$query = "INSERT INTO `" . $db . "`.`" . $table . "`";
	if ($input->body)
	{
		$query .= " SET ";
		foreach($input->body as $key => $value)
			if (!in_array("ignored", $raw_resource->$key->post))
				if (strpos($key, '.') AND $value)
					$query .= substr($key, 0, strpos($key,'.')) . " = JSON_SET(`" . substr($key, 0, strpos($key,'.')) . "`, '$." . substr($key, strpos($key,'.') + 1) . "', :" . str_replace('.', '_', $key) . "),";
				else if (strpos($key, '.') AND !$value)
					$query .= substr($key, 0, strpos($key,'.')) . " = JSON_REMOVE(`" . substr($key, 0, strpos($key,'.')) . "`, '$." . substr($key, strpos($key,'.') + 1) . "'),";
				else
					$query .= '`' . $key . '`=:' . $key . ',';
		$query = substr($query,0,-1);
	}
	else
		$query .= " VALUES ()";

	$prepare = $connection->prepare($query);
	foreach($input->body as $key => $value)
		if (!in_array("ignored", $raw_resource->$key->post))
			if (strpos($key, '.') AND $input->body->$key)
				$prepare->bindParam(':' . str_replace('.', '_', $key), $input->body->$key, PDO::PARAM_STR);
			else if (!strpos($key, '.'))
				$prepare->bindParam(':'. $key, $input->body->$key, $input->pdotype->$key);
	
	return $prepare;
}

function datatables_update($connection, $resource, $db, $table, $id)
{
	global $input;
	
	$raw_resource = (object)array_filter((array)$resource, function($column){return (!$column->reference AND !$column->virtual);});
	
	$query = "UPDATE `" .  $db . "`.`" . $table . "` SET ";
	foreach($input->body as $key => $value)
		if (!in_array("immutable", $resource->$key->patch))
			if (strpos($key, '.') AND $input->body->$key)
				$query .= substr($key, 0, strpos($key,'.')) . " = JSON_SET(`" . substr($key, 0, strpos($key,'.')) . "`, '$." . substr($key, strpos($key,'.') + 1) . "', :" . str_replace('.', '_', $key) . "),";
			else if (strpos($key, '.') AND !$input->body->$key)
				$query .= substr($key, 0, strpos($key,'.')) . " = JSON_REMOVE(`" . substr($key, 0, strpos($key,'.')) . "`, '$." . substr($key, strpos($key,'.') + 1) . "'),";
			else
				$query .= '`' . $key . '`=:' . $key . ',';
	$query = substr($query,0,-1);
	$query .= " WHERE id = '" . $id . "'";

	$prepare = $connection->prepare($query);
	foreach($input->body as $key => $value)
		if (!in_array("immutable", $resource->$key->patch))
			if (strpos($key, '.') AND $input->body->$key)
				$prepare->bindParam(':' . str_replace('.', '_', $key), $input->body->$key, PDO::PARAM_STR);
			else
				$prepare->bindParam(':'. $key, $input->body->$key, $input->pdotype->$key);
	return $prepare;
}