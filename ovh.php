<?php
function ovh_api_request($type, $endpoint, $data = '')
{
	if (!getenv('OVH_SECRET_KEY'))
		return error_log('OVH API cannot be reached (OVH credentials missing)');
	$api_url = 'https://api.ovh.com/1.0';
	$timestamp = file_get_contents("https://api.ovh.com/1.0/auth/time");
	$signature = getenv('OVH_SECRET_KEY') . '+' . getenv('OVH_CONSUMER_KEY') . '+' . $type . '+' . $api_url . $endpoint . '+' . $data . '+' . $timestamp;
	$sigkey = '$1$' . sha1($signature);

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $api_url . $endpoint);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	curl_setopt(
		$curl,
		CURLOPT_HTTPHEADER,
		[
			'Content-Type:application/json;charset=utf-8',
			'X-Ovh-Application:' . getenv('OVH_APP_KEY'),
			'X-Ovh-Consumer:' . getenv('OVH_CONSUMER_KEY'),
			'X-Ovh-Timestamp:' . $timestamp,
			'X-Ovh-Signature:' . $sigkey
		]
	);
	$response = curl_exec($curl);
	curl_close($curl);
	return json_decode($response);
}

function ovh_dns_record_replace($type, $domain, $subdomain, $target)
{
	if (!getenv('OVH_SECRET_KEY'))
		return error_log('DNS ' . $type . ' RECORD cannot be added automatically for ' . $subdomain .  '.' . $domain . ' (OVH credentials missing)');
	
	$records = ovh_api_request('GET', '/domain/zone/' . $domain . '/record?fieldType=' . $type . '&subDomain=' . $subdomain);
	
	if (!is_array($records)) 
		return error_log('DNS ' . $type . ' RECORD cannot be added automatically for ' . $subdomain .  '.' . $domain . ' (domain not found in OVH API)');

	if (sizeof($records) == 1) 
	{
		$record = ovh_api_request('GET', '/domain/zone/' . $domain . '/record/' . $records[0]);
		if ($record->target != $target) 
		{
			error_log('DNS ' . $type . ' A RECORD updated for ' . $subdomain .  '.' . $domain);
			ovh_api_request('PUT', '/domain/zone/' . $domain . '/record/' . $records[0], '{"subDomain": "' . $subdomain . '", "target": "' . $target  . '", "ttl": ' . ($subdomain=='mail'?60:0) . '}');
		} 
		else
			error_log('DNS ' . $type . ' RECORD already set for ' . $subdomain .  '.' . $domain);
	} 
	else 
	{
		foreach ($records as $record)
			ovh_api_request('DELETE', '/domain/zone/' . $domain . '/record/' . $record->id);
		error_log('DNS ' . $type . ' RECORD replaced for ' . $subdomain . '.' . $domain);
		$result = ovh_api_request('POST', '/domain/zone/' . $domain . '/record/', '{"fieldType": "' . $type . '", "subDomain": "' . $subdomain . '", "target": "' . $target . '", "ttl": ' . ($subdomain=='mail'?60:0) . '}');
		error_log(json_encode($result));
	}
}

function ovh_set_ip_reverse($ip, $domain)
{
	if (!getenv('OVH_SECRET_KEY'))
		return error_log('REVERSE DNS for ' . $ip . ' cannot be set automatically for ' . $domain . ' (OVH credentials missing)');
	
	$records = ovh_api_request('GET', '/ip/' . $ip . '/reverse/' . $ip);
	
	if ($records->message) 
		return error_log('REVERSE DNS FOR ' . $ip . ' : cannot be pointed automatically TO ' . $domain . ' (OVH API says : ' . $records->message . ')');

	if ($records->reverse == $domain . '.')
		error_log('REVERSE DNS FOR ' . $ip . ' : ALREADY POINTING TO ' . $domain);
	else
	{
		$response = ovh_api_request('POST', '/ip/' . $ip . '/reverse', '{"ipReverse": "' . $ip . '", "reverse": "' . $domain . '"}');
		if ($response->reverse == $domain . '.')
			error_log('REVERSE DNS FOR ' . $ip . ' : CHANGED TO ' . $domain);
		else
			error_log('ERROR CHANGING REVERSE DNS RECORD FOR ' . $ip . ' : ' . json_encode($response));
	}
}
?>