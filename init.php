<?php
include_once '/srv/api/libs/JWT.php';
use optimus\JWT;

include_once '/srv/api/libs/websocket_client.php';
use optimus\OptimusWebsocket;

if (file_exists('/srv/api/libs/docker_socket.php'))
	include_once '/srv/api/libs/docker_socket.php';

//READ MANIFEST.JSON
$manifest_source = file_get_contents('/srv/manifest.json');
if (!$manifest_source)
	die("Unable to open /srv/manifest.json\n");
$manifest = json_decode($manifest_source);

if (!$manifest->name)
	die("invalid manifest.json\n");

//GET CONTAINER LOCAL IP
$container_ip = trim(shell_exec('hostname -i'));

error_log('=== ' . strtoupper($manifest->name) . " V" . $manifest->version_display . (getenv('DEV')==1 ? ' (DEV MODE)' : '') . " ===");

//WAIT FOR OPTIMUS-DATABASES TO BE READY
if (!file_exists('/run/mysqld/mysqld.sock') OR time() < filemtime('/run/mysqld/mysqld.sock') + 5)
	exit;

//KILL OLD CONTAINER IF EXISTS
if ($manifest->name == 'optimus-base')
{
	docker_socket_request('POST /v1.41/containers/optimus-base-old/kill');
	docker_socket_request('DELETE /v1.41/containers/optimus-base-old');
}

try 
{
	$mariadb_root_user = getenv('MARIADB_ROOT_USER');
	$mariadb_root_password = getenv('MARIADB_ROOT_PASSWORD');
	$connection = new PDO("mysql:host=optimus-databases", $mariadb_root_user, $mariadb_root_password, array(PDO::ATTR_TIMEOUT => 30));
	if ($connection) 
	{
		error_log("Connection to database established !");

		//GET PUBLIC IP
		$public_ip = file_get_contents('https://ipinfo.io/ip');

		//CHECK LAST INSTALLED VERSION TO DETERMINE IF DATABASE UPGRAGE IS NEEDED
		$server = $connection->query("SHOW DATABASES LIKE 'server'");
		if ($server->rowCount() == 1) 
		{
			//CHECK IS SERVICE IS ALREADY REGISTERED
			$last_installed_version = $connection->query("SELECT JSON_VALUE(manifest, '$.version_date') as version_date FROM `server`.`services` WHERE name = '" . $manifest->name . "'");
			//IF SERVICE EXISTS, FETCH DATA
			if ($last_installed_version->rowCount() > 0)
				$last_installed_version = $last_installed_version->fetch(PDO::FETCH_ASSOC)['version_date'];
			else 
				$last_installed_version = 0;
		}
		//IF SERVER DATABASE DOESN'T EXISTS, IT MEANS WE ARE CURRENTLY INSTALLING BASE SERVICE
		else
		{
			error_log("Server database not detected");
			$last_installed_version = 0;
		}

		//ACTIVATE WEBSOCKET CLIENT IF SERVICE IS NOT OPTIMUS-BASE
		if (is_dir('/srv/sql') AND $manifest->name != 'optimus-base')
			$websocket = new OptimusWebsocket(1, 'optimus-init@' . getenv('DOMAIN'), 1);

		//DATABASES UPDATE
		//$db_types_to_update = $connection->query("SELECT id, value FROM common.users_types")->fetchAll(PDO::FETCH_KEY_PAIR);
		//$db_types_to_update = array_merge(array('service'), $db_types_to_update);
		$db_types_to_update = array('service', 'user', 'business');
		//COUNT TOTAL NUMBER OF SQL INSTRUCTIONS TO EXECUTE
		if ($manifest->name == 'optimus-base' AND $last_installed_version == 0)
			$total_instructions = 100;
		else
			foreach($db_types_to_update as $id => $type)
				if (is_dir('/srv/sql/' . $type)) 
				{
					$sql_files = array_diff(scandir('/srv/sql/' . $type), array('..', '.'));
					if (substr(end($sql_files),0,-4) > $last_installed_version)
					{
						if ($type == 'service')
							$users = array(1);
						else if ($manifest->name == 'optimus-base')
							$users = $connection->query("SELECT id FROM `server`.`users` WHERE type = '" . $id . "' ORDER BY id")->fetchAll(PDO::FETCH_COLUMN);
						else
							$users = $connection->query("SELECT users.id FROM `server`.`users_services`, server.users WHERE users_services.user = users.id AND users.type = '" . $id . "' AND service = '" . $manifest->name . "' ORDER BY user")->fetchAll(PDO::FETCH_COLUMN);
						foreach($users as $user) 
							foreach ($sql_files as $sql_file)
								if (substr($sql_file,0,-4) > $last_installed_version) 
								{
									$sql = file_get_contents("/srv/sql/" . $type . "/" . $sql_file);
									$sql = explode(';', $sql);
									$total_instructions += sizeof($sql);
								}
					}
				}
		//EXECUTE SQL INSTRUCTIONS
		foreach($db_types_to_update as $id => $type)
			if (is_dir('/srv/sql/' . $type)) 
			{
				$sql_files = array_diff(scandir('/srv/sql/' . $type), array('..', '.'));
				if (substr(end($sql_files),0,-4) > $last_installed_version)
				{
					if ($type == 'service')
						$users = array(1);
					else if ($manifest->name == 'optimus-base')
						$users = $connection->query("SELECT id FROM `server`.`users` WHERE type = '" . $id . "' ORDER BY id")->fetchAll(PDO::FETCH_COLUMN);
					else
						$users = $connection->query("SELECT users.id FROM `server`.`users_services`, server.users WHERE users_services.user = users.id AND users.type = '" . $id . "' AND service = '" . $manifest->name . "' ORDER BY user")->fetchAll(PDO::FETCH_COLUMN);
					foreach($users as $user) 
					{
						if ($type == 'service')
							error_log("SERVICE UPDATE FROM " . $last_installed_version . " to " . substr(end($sql_files),0,-4));
						else
						{
							error_log("USER " .  $user . " UPDATE FROM " . $last_installed_version . " to " . substr(end($sql_files),0,-4));
							$connection->exec("USE `user_" . $user . "`");
						}
						foreach ($sql_files as $sql_file)
							if (substr($sql_file,0,-4) > $last_installed_version) 
							{
								error_log("==> Start processing file " .  $sql_file);
								$sql = file_get_contents("/srv/sql/" . $type . "/" . $sql_file);
								$sql = explode(';', $sql);
								$y = 0;
								$chunk = round(ceil(sizeof($sql) / 100));
								foreach ($sql as $instruction) 
								{
									$y++;
									$executed_instructions++;
									if ($instruction != '')
										if ($connection->query($instruction)) 
										{
											if ($y % $chunk == 0)
												error_log("Processing file " .  $sql_file . " : " . round($y / sizeof($sql) * 100) . "%");
										}
										else
										{
											error_log("ERROR : " . $connection->errorInfo()[2]);
											if ($connection->errorInfo()[2] == "MySQL server has gone away")
												exit;
										}
								}
								error_log("Processing file " .  $sql_file . " : 100%");
								if ($websocket)
									$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $manifest->name, "progress" => (45 + round(50 * $executed_instructions / $total_instructions))))));
							}
					}
				}
			}
	
		//CONFIGURE SQL API USER
		$users = $connection->prepare("SELECT User, Host FROM mysql.user WHERE user = :user");
		$users->bindParam(':user', $manifest->name, PDO::PARAM_STR);
		$users->execute();
		$users = $users->fetchAll(PDO::FETCH_OBJ);
		foreach($users as $user)
			if (!$connection->query("DROP USER `" . $manifest->name . "`@`" . $user->Host . "`"))
				error_log("Error while removing MARIADB user " . $manifest->name . "`@`" . $user->Host . " from database");
		putenv("MARIADB_API_USER=" . $manifest->name);
		putenv("MARIADB_API_PASSWORD=" . base64_encode(openssl_encrypt('password_for_' . $manifest->name, 'aes-128-ecb', getenv('AES_KEY'))));
		$grants = file_get_contents('/srv/sql/grants.sql');
		$grants = str_replace('{MARIADB_API_USER}', getenv('MARIADB_API_USER'), $grants);
		$grants = str_replace('{MARIADB_API_PASSWORD}', getenv('MARIADB_API_PASSWORD'), $grants);
		$grants = str_replace('{MARIADB_ROOT_PASSWORD}', getenv('MARIADB_ROOT_PASSWORD'), $grants);
		$grants = str_replace('{CONTAINER_IP}', $container_ip, $grants);
		$grants = explode(';', $grants);
		foreach ($grants as $instruction)
			if ($instruction != '')
				if (!$connection->query($instruction))
					error_log($connection->errorInfo()[2]);
		error_log('MARIADB user ' . $manifest->name . ' successfully added');
	}

	//ADDING NGINX MAIL
	if (is_dir('/srv/vhosts/mail')) 
	{
		include_once '/srv/api/libs/ovh.php';
		$files = scandir('/srv/vhosts/mail');
		foreach ($files as $file)
			if (!in_array($file, array(".", ".."))) 
			{
				$file_content = file_get_contents('/srv/vhosts/mail/' . $file);
				$file_content = str_replace('{DOMAIN}', getenv('DOMAIN'), $file_content);
				$file_content = str_replace('{CONTAINER_IP}', $container_ip, $file_content);
				$file_content = str_replace('{NAME}', $manifest->name, $file_content);
				file_put_contents('/srv/volumes/vhosts/mail/' . $file . '.' . getenv('DOMAIN'), $file_content);
				chown('/srv/volumes/vhosts/mail/' . $file . '.' . getenv('DOMAIN'), 'www-data');
				chgrp('/srv/volumes/vhosts/mail/' . $file . '.' . getenv('DOMAIN'), 'www-data');
				error_log("MAILSERVER " . $file . '.' . getenv('DOMAIN') . " added to NGINX");
				ovh_dns_record_replace('A', getenv('DOMAIN'), $file, $public_ip);
				ovh_dns_record_replace('A', getenv('DOMAIN'), 'imap', $public_ip);
				ovh_dns_record_replace('A', getenv('DOMAIN'), 'smtp', $public_ip);
				$mx_records = ovh_api_request('GET', '/domain/zone/' . getenv('DOMAIN') . '/record?fieldType=MX');
				foreach ($mx_records as $mx_record)
					ovh_api_request('DELETE', '/domain/zone/' . getenv('DOMAIN') . '/record/' . $mx_record);
				ovh_dns_record_replace('MX', getenv('DOMAIN'), '', '0 mail.' . getenv('DOMAIN') . '.');
				ovh_dns_record_replace('SRV', getenv('DOMAIN'), '_imap._tcp', '0 0 143 imap.' . getenv('DOMAIN') . '.');
				ovh_dns_record_replace('SRV', getenv('DOMAIN'), '_imaps._tcp', '0 0 993 imap.' . getenv('DOMAIN') . '.');
				ovh_dns_record_replace('SRV', getenv('DOMAIN'), '_submission._tcp', '0 0 587 smtp.' . getenv('DOMAIN') . '.');
				ovh_dns_record_replace('SRV', getenv('DOMAIN'), '_submissions._tcp', '0 0 587 smtp.' . getenv('DOMAIN') . '.');
				ovh_dns_record_replace('SRV', getenv('DOMAIN'), '_autodiscover._tcp', '0 0 443 mail.' . getenv('DOMAIN') . '.');
				ovh_dns_record_replace('CNAME', getenv('DOMAIN'), 'autodiscover', 'mail.' . getenv('DOMAIN') . '.');
				ovh_dns_record_replace('SPF', getenv('DOMAIN'), '', 'v=spf1 mx include:mx.ovh.com ~all');
				ovh_dns_record_replace('DMARC', getenv('DOMAIN'), '_dmarc', 'v=DMARC1;p=quarantine;sp=quarantine;pct=100;adkim=r;aspf=r;fo=1;ri=86400;rua=mailto:postmaster@' . getenv('DOMAIN') . ';ruf=mailto:postmaster@' . getenv('DOMAIN') . ';rf=afrf');
				
				if (!is_dir('/etc/dkim/keys/' . getenv('DOMAIN')))
				{
					error_log('Adding ' . getenv('DOMAIN') . ' to the mailserver and creating new DKIM signature');
					$connection->query("INSERT IGNORE INTO `mailserver`.`mailboxes_domains` SET status = b'1', domain = '" . getenv('DOMAIN') . "'");
					shell_exec('mkdir -p /etc/dkim/keys/' . getenv('DOMAIN'));
					shell_exec('opendkim-genkey -D /etc/dkim/keys/' . getenv('DOMAIN') . ' -d ' . getenv('DOMAIN') . ' -s mail');
					file_put_contents('/etc/dkim/KeyTable', 'mail._domainkey.' . getenv('DOMAIN') . ' ' . getenv('DOMAIN') . ':mail:/etc/dkim/keys/' . getenv('DOMAIN') . '/mail.private' . "\n", FILE_APPEND);
					file_put_contents('/etc/dkim/SigningTable', '*@' . getenv('DOMAIN') . ' mail._domainkey.' . getenv('DOMAIN') . "\n", FILE_APPEND);
					file_put_contents('/etc/dkim/TrustedHosts', getenv('DOMAIN') . "\n", FILE_APPEND);
					shell_exec('chown opendkim:opendkim -R /etc/dkim');
				}
				$dkim_record = file_get_contents('/etc/dkim/keys/' . getenv('DOMAIN') . '/mail.txt');
				$dkim_record = substr($dkim_record, strpos($dkim_record, 'v='));
				$dkim_record = substr($dkim_record, 0, strpos($dkim_record, ')'));
				$dkim_record = str_replace("\t",'', $dkim_record);
				$dkim_record = str_replace('"','', $dkim_record);
				$dkim_record = str_replace(' ','', $dkim_record);
				$dkim_record = str_replace(chr(13),'', $dkim_record);
				$dkim_record = str_replace(chr(10),'', $dkim_record);
				ovh_dns_record_replace('DKIM', getenv('DOMAIN'), 'mail._domainkey', $dkim_record);
				sleep(4);
				ovh_set_ip_reverse($public_ip, 'mail.' . getenv('DOMAIN'));
			}
		if (file_exists('/srv/default.sieve'))
			shell_exec('mv /srv/default.sieve /srv/mailboxes/default.sieve');
	}

	//ROUNDCUBE WEBMAIL
	if ($manifest->name == 'optimus-webmail')
	{
		$smtp = $connection->query("SELECT value FROM `server`.`preferences` WHERE id = 'smtp'")->fetch(PDO::FETCH_OBJ);
		putenv('SMTP=' . ($smtp ? $smtp->value : 'local'));
		if (!$smtp)
			$connection->query("REPLACE INTO `server`.`preferences` SET id = 'smtp', value = 'local'");
		
		include_once '/srv/api/libs/ovh.php';
		if (getenv('SMTP') == 'ovh')
		{
			$dkim = ovh_api_request('GET', '/email/domain/' . getenv('DOMAIN') . '/dkim');
			if ($dkim->status != 'enabled')
			{
				error_log("Enabling DKIM for OVH MX PLAN");
				ovh_api_request('PUT', '/email/domain/' . getenv('DOMAIN') . '/dkim/enable');
			}
			else
				error_log("DKIM already enabled for OVH MX PLAN");
				
			//peut être supprimé à long terme car les boites sont dorénavant générés lorsque l'utilisateur active le service mail
			$users = $connection->query("SELECT users.id, users.email FROM `server`.`users_services`, server.users WHERE users_services.user = users.id AND users_services.service = 'optimus-mail' ORDER BY users.id")->fetchAll(PDO::FETCH_OBJ);
			foreach($users as $user)
			{
				error_log("Creating mailbox " . $user->email . " for OVH MX PLAN");
				$mail_password = substr(base64_encode(openssl_encrypt('ovh_smtp_' . getenv('DOMAIN'), 'aes-128-ecb', getenv('AES_KEY'))), 0, 24);
				$mailbox = ovh_api_request('POST', '/email/domain/' . getenv('DOMAIN') . '/account', '{"accountName": "' . explode('@',$user->email)[0] . '", "password": "' . $mail_password . '", "size": 2500000}');
			}
		}
	}

	//ADDING NGINX STREAMS
	if (is_dir('/srv/vhosts/streams')) 
	{
		$files = scandir('/srv/vhosts/streams');
		foreach ($files as $file)
			if (!in_array($file, array(".", ".."))) 
			{
				$file_content = file_get_contents('/srv/vhosts/streams/' . $file);
				$file_content = str_replace('{DOMAIN}', getenv('DOMAIN'), $file_content);
				$file_content = str_replace('{CONTAINER_IP}', $container_ip, $file_content);
				$file_content = str_replace('{NAME}', $manifest->name, $file_content);
				file_put_contents('/srv/volumes/vhosts/streams/' . $file . '.' . getenv('DOMAIN'), $file_content);
				chown('/srv/volumes/vhosts/streams/' . $file . '.' . getenv('DOMAIN'), 'www-data');
				chgrp('/srv/volumes/vhosts/streams/' . $file . '.' . getenv('DOMAIN'), 'www-data');
				error_log("STREAMS " . $file . '.' . getenv('DOMAIN') . " added to NGINX");
			}
	}

	//ADDING NGINX VHOSTS SERVERS
	if (is_dir('/srv/vhosts/servers')) 
	{
		include_once '/srv/api/libs/ovh.php';
		$files = scandir('/srv/vhosts/servers');
		foreach ($files as $file)
			if (!in_array($file, array(".", ".."))) 
			{
				$file_content = file_get_contents('/srv/vhosts/servers/' . $file);
				$file_content = str_replace('{DOMAIN}', getenv('DOMAIN'), $file_content);
				$file_content = str_replace('{CONTAINER_IP}', $container_ip, $file_content);
				$file_content = str_replace('{NAME}', $manifest->name, $file_content);
				
				//OPTIMUS-WEBSITE - START
				$config = $connection->query("SELECT value FROM `server`.`preferences` WHERE id = 'website'")->fetch(PDO::FETCH_OBJ);
				$config = explode(' ', $config->value);
				if ($config[0] == 'proxypass' AND $config[1])
				{
					error_log("WEBSITE PROXYPASS TO " . $config[1]);
					$location = <<<LOCATION
					location /
						{
							proxy_set_header Host \$host;
							proxy_set_header X-Real-IP \$remote_addr;
							resolver 1.1.1.1;
							set \$redirect_to $config[1];
							proxy_pass \$redirect_to;
							client_max_body_size 4G;
						}
					LOCATION;
				}
				else if ($config[0] == '301')
				{
					error_log("WEBSITE REDIRECTING TO $config[1]");
					$location = <<<LOCATION
					location /
						{
							add_header Cache-Control "no-store";
							return 301 $config[1];
						}
					LOCATION;
				}
				else if ($config[0] == 'local')
				{
					error_log("WEBSITE REDIRECTING TO /srv/www");
					$location = <<<LOCATION
					root /srv/www;

						location /
						{
							try_files \$uri /index.php\$is_args\$args;
						}

						location ~ \.php
						{
							fastcgi_pass $container_ip:9000;
							fastcgi_split_path_info ^(.+\.php)(/.*)$;
							include fastcgi_params;
							fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
							fastcgi_param HTTPS off;
						}
					LOCATION;
				}
				else if ($config[0] == 'work-in-progress')
				{
					error_log("WEBSITE REDIRECTING TO 404");
					$location = <<<LOCATION
					error_page 404 /work-in-progress.html;
						location /work-in-progress.html
						{
							root /srv/optimus/optimus-website/client;
						}
						location /
						{
							return 404;
						}
					LOCATION;
				}
				else
				{
					error_log("WEBSITE REDIRECTING TO 404");
					$location = <<<LOCATION
					error_page 404 /404.html;
						location /404.html
						{
							root /srv/optimus/optimus-website/client;
						}
						location /
						{
							return 404;
						}
					LOCATION;
				}
				$file_content = str_replace('{LOCATION}', $location, $file_content);
				//OPTIMUS-WEBSITE - END

				file_put_contents('/srv/volumes/vhosts/servers/' . $file . '.' . getenv('DOMAIN'), $file_content);
				chown('/srv/volumes/vhosts/servers/' . $file . '.' . getenv('DOMAIN'), 'www-data');
				chgrp('/srv/volumes/vhosts/servers/' . $file . '.' . getenv('DOMAIN'), 'www-data');
				error_log("SERVER " . $file . '.' . getenv('DOMAIN') . " added to NGINX");
				ovh_dns_record_replace('A', getenv('DOMAIN'), $file, $public_ip);
			}
	}

	//ADDING NGINX VHOSTS LOCATIONS
	if (is_dir('/srv/vhosts/locations')) 
	{
		$files = scandir('/srv/vhosts/locations');
		foreach ($files as $file)
			if (!in_array($file, array(".", ".."))) 
			{
				$file_content = file_get_contents('/srv/vhosts/locations/' . $file);
				$file_content = str_replace('{DOMAIN}', getenv('DOMAIN'), $file_content);
				$file_content = str_replace('{CONTAINER_IP}', $container_ip, $file_content);
				$file_content = str_replace('{NAME}', $manifest->name, $file_content);
				file_put_contents('/srv/volumes/vhosts/locations/' . $file . '.' . getenv('DOMAIN') . '.' . $manifest->name, $file_content);
				chown('/srv/volumes/vhosts/locations/' . $file . '.' . getenv('DOMAIN') . '.' . $manifest->name, 'www-data');
				chgrp('/srv/volumes/vhosts/locations/' . $file . '.' . getenv('DOMAIN') . '.' . $manifest->name, 'www-data');
				error_log("LOCATIONS for " . $file . '.' . getenv('DOMAIN') . " added to NGINX");
			}
	}

	//REFRESH DNS
	if (function_exists('ovh_api_request'))
		ovh_api_request('POST', '/domain/zone/' . getenv('DOMAIN') . '/refresh');

	//ENVSUBST
	if ($manifest->envsubst)
		foreach($manifest->envsubst as $path)
		{
			$file_content = file_get_contents($path);
			$file_content = str_replace('{MARIADB_API_USER}', getenv('MARIADB_API_USER'), $file_content);
			$file_content = str_replace('{MARIADB_API_PASSWORD}', getenv('MARIADB_API_PASSWORD'), $file_content);
			$file_content = str_replace('{DOMAIN}', getenv('DOMAIN'), $file_content);
			$file_content = str_replace('{CONTAINER_IP}', $container_ip, $file_content);
			$file_content = str_replace('{PUBLIC_IP}', $public_ip, $file_content);
			$file_content = str_replace('{AES_KEY}', getenv('AES_KEY'), $file_content);
			$file_content = str_replace('{API_SHA_KEY}', getenv('API_SHA_KEY'), $file_content);
			$file_content = str_replace('{DES_KEY}', substr(base64_encode(openssl_encrypt('des_key_for' . $manifest->name, 'aes-128-ecb', getenv('AES_KEY'))),0,24), $file_content);
			file_put_contents($path, $file_content);
		}

	//REMOVING UNUSED PASSWORDS FROM PHP ENVIRONMENT
	if (getenv('KEEP_MARIADB_ROOT_ACCESS')!=1)
	{
		putenv('MARIADB_ROOT_USER=');
		putenv('MARIADB_ROOT_PASSWORD=');
	}

	if (getenv('KEEP_OVH_ACCESS')!=1)
	{
		putenv('OVH_APP_KEY=');
		putenv('OVH_SECRET_KEY=');
		putenv('OVH_CONSUMER_KEY=');
	}

	//LAUNCH WEBSOCKET SERVER IF REQUIRED
	if (getenv('WEBSOCKET_SERVER') == 1) 
	{
		error_log("Service launching WEBSOCKET SERVER on port 20000");
		echo shell_exec('php /srv/api/libs/websocket_server.php > /dev/null &');
	}

	//EXPORT CLIENT FILES
	if (is_dir('/srv/volumes/optimus/' . $manifest->name . '/.git'))
		error_log('In order to protect your work from unwanted actions, container doesn\'t modify /srv/optimus/' . $manifest->name . ' folder unless you remove the git repo manually !');
	else
	{
		if (!is_dir('/srv/volumes/optimus/' . $manifest->name))
			shell_exec('mkdir /srv/volumes/optimus/' . $manifest->name);
		if (is_dir('/srv/client'))
		{	
			if (is_dir('/srv/volumes/optimus/' . $manifest->name . '/client'))
				shell_exec('rm -R /srv/volumes/optimus/' . $manifest->name . '/client');
			shell_exec('cp -R /srv/client /srv/volumes/optimus/' . $manifest->name);
		}
	}
	
	//MET A JOUR LE SERVICE WORKER
	if ($manifest->name == 'optimus-base')
	{
		$service_worker = file_get_contents('/srv/volumes/optimus/optimus-base/client/serviceworker.js');
		$service_worker = substr($service_worker, 32);
		$service_worker = 'const buildDate = ' . date('YmdHis') . $service_worker;
		file_put_contents('/srv/volumes/optimus/optimus-base/client/serviceworker.js', $service_worker);
	}

	//EXPORT WEBSOCKET FILES
	if (is_dir("/srv/api/websocket"))
		shell_exec('cp /srv/api/websocket/*.php /srv/volumes/websocket');

	//CLEANUP SCRIPT
	function cleanup()
	{
		global $mariadb_root_user, $mariadb_root_password, $manifest;
		$connection = new PDO("mysql:host=optimus-databases", $mariadb_root_user , $mariadb_root_password, array(PDO::ATTR_TIMEOUT => 30));
		error_log("Initiating service cleanup process... ");

		//REMOVING NGINX VHOSTS MAILSERVERS
		if (is_dir('/srv/vhosts/mail')) 
		{
			$files = scandir('/srv/vhosts/mail');
			foreach ($files as $file)
				if (!in_array($file, array(".", "..")))
				{
					unlink('/srv/volumes/vhosts/mail/' . $file . '.' . getenv('DOMAIN'));
					error_log("MAILSERVER " . $file . '.' . getenv('DOMAIN') . " removed from NGINX");
				}
		}

		//REMOVING NGINX VHOSTS SERVERS
		if (is_dir('/srv/vhosts/servers')) 
		{
			$files = scandir('/srv/vhosts/servers');
			foreach ($files as $file)
				if (!in_array($file, array(".", "..")))
				{
					unlink('/srv/volumes/vhosts/servers/' . $file . '.' . getenv('DOMAIN'));
					error_log("SERVER " . $file . '.' . getenv('DOMAIN') . " removed from NGINX");
				}
		}

		//REMOVING NGINX VHOSTS LOCATIONS
		if (is_dir('/srv/vhosts/locations')) 
		{
			$files = scandir('/srv/vhosts/locations');
			foreach ($files as $file)
				if (!in_array($file, array(".", "..")))
				{
					unlink('/srv/volumes/vhosts/locations/' . $file . '.' . getenv('DOMAIN') . '.' . $manifest->name);
					error_log("LOCATIONS for " . $file . '.' . getenv('DOMAIN') . " removed from NGINX");
				}
		}

		//REMOVING WEBSOCKET FILES
		if (is_dir('/srv/api/websocket')) 
		{
			$files = scandir('/srv/api/websocket');
			foreach ($files as $file)
				if (!in_array($file, array(".", "..")))
				{
					unlink('/srv/volumes/websocket/' . $file);
					error_log("WEBSOCKET FILES removed");
				}
		}

		//SET SERVICE STATUS = 0 IN THE DATABASE
		if ($connection->query("UPDATE `server`.`services` SET status = b'0' WHERE name = '" . $manifest->name . "'"))
			error_log("Service unregistered successfully");
		else
			die(error_log("Error while unregistering service : " .  $connection->errorInfo()[2]));
		
		//REMOVE USER FROM DATABASE
		$users = $connection->prepare("SELECT User, Host FROM mysql.user WHERE user = :user");
		$users->bindParam(':user', $manifest->name, PDO::PARAM_STR);
		$users->execute();
		$users = $users->fetchAll(PDO::FETCH_OBJ);
		foreach($users as $user)
			if ($connection->query("DROP USER `" . $manifest->name . "`@`" . $user->Host . "`"))
				error_log("MARIADB user " . $manifest->name . "`@`" . $user->Host . " successfully removed from database");
			else
				error_log("Error while removing MARIADB user " . $manifest->name . "`@`" . $user->Host . " from database");
		

		//EXIT MAIN PROCESS (PID 1) WHICH STOPS THE CONTAINER
		error_log("Service stopped successfully");
		exit;
	}
	pcntl_async_signals(true);
	pcntl_signal(SIGTERM, "cleanup");

	//CRON
	if (is_file('/srv/cron.php'))
		include ('/srv/cron.php');

	//REGISTER SERVICE
	if ($connection->query("REPLACE INTO `server`.`services` SET name = '" . $manifest->name . "', status = b'1', manifest = '" . addslashes(json_encode($manifest, JSON_UNESCAPED_UNICODE)) . "'"))
		error_log("Service registered successfully on ip " . $container_ip);
	else
		die(error_log("Error while registering service : " .  $connection->errorInfo()[2]));

	$connection = null;

	//COMMANDS
	if ($manifest->commands)
		foreach($manifest->commands as $command)
			shell_exec($command);

	//INFINITE LOOP THAT ALLOWS TO WAIT FOR SIGTERM SIGNAL
	while (true) 
	{
		sleep(1);
		if (function_exists('cron'))
			cron();
	}
} 
catch (PDOException $e) 
{
	error_log("Error connecting to database: " . $e->getMessage());
}
