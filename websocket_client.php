<?php
namespace optimus;
use optimus\JWT;

class OptimusWebsocket
{
	private $socket = null;

	public function __construct($user_id, $user_email, $user_admin) 
	{ 
		$this->connect($user_id, $user_email, $user_admin);
	}
	
	public function __destruct()
	{
		$message = $this->hybi10Encode($message, 'close');
		fwrite($this->socket, $message);
		fclose($this->socket);
	}

	public function connect($user_id, $user_email, $user_admin = 0)
	{
		$jwt = new JWT(getenv('API_SHA_KEY'), 'HS512', 3600, 10);
		$token = $jwt->encode(["user" => array("id" => $user_id, "email" => $user_email), "admin" => $user_admin, "aud" => "http://" . getenv('DOMAIN'), "scopes" => ['user'], "iss" => "http://" . getenv('DOMAIN')]);
		
		$handshake_header = "GET / HTTP/1.1\r\n".
		"Host: api." . getenv('DOMAIN') . ":20000\r\n".
		"Upgrade: websocket\r\n".
		"Connection: Upgrade\r\n".
		"Sec-WebSocket-Key: ". uniqid() . "\r\n".
		"Origin: api." . getenv('DOMAIN') . "\r\n".
		"Sec-WebSocket-Version: 13\r\n".
		"Cookie: token=" . $token . "\r\n\r\n";

		$this->socket = fsockopen("tls://api." . getenv('DOMAIN'), 20000, $errno, $errstr, 2);
		fwrite($this->socket, $handshake_header);

		return true;
	}

	public function send($message)
	{
		$message = $this->hybi10Encode($message, 'text');
		fwrite($this->socket, $message);
	}

	public function hybi10Encode($payload, $type = 'text') 
	{
		$frameHead = array();
		$frame = '';
		$payloadLength = strlen($payload);

		if ($type == 'text')
			$frameHead[0] = 129;
		else if ($type == 'close')
			$frameHead[0] = 136;
		else if ($type == 'ping')
			$frameHead[0] = 137;
		else if ($type == 'pong')
			$frameHead[0] = 138;

		if ($payloadLength > 65535) 
		{
			$payloadLengthBin = str_split(sprintf('%064b', $payloadLength), 8);
			$frameHead[1] = 255;
			for ($i = 0; $i < 8; $i++)
				$frameHead[$i + 2] = bindec($payloadLengthBin[$i]);

			if ($frameHead[2] > 127) 
			{
				$this->close(1004);
				return false;
			}
		} 
		elseif ($payloadLength > 125) 
		{
			$payloadLengthBin = str_split(sprintf('%016b', $payloadLength), 8);
			$frameHead[1] = 254;
			$frameHead[2] = bindec($payloadLengthBin[0]);
			$frameHead[3] = bindec($payloadLengthBin[1]);
		} 
		else
			$frameHead[1] = $payloadLength + 128;

		foreach (array_keys($frameHead) as $i)
			$frameHead[$i] = chr($frameHead[$i]);

		$mask = array();
		for ($i = 0; $i < 4; $i++)
			$mask[$i] = chr(rand(0, 255));
		$frameHead = array_merge($frameHead, $mask);

		$frame = implode('', $frameHead);

		for ($i = 0; $i < $payloadLength; $i++) 
			$frame .= $payload[$i] ^ $mask[$i % 4];

		return $frame;
	}
}
?>