<?php
include_once '/srv/api/libs/JWT.php';
use optimus\JWT;

function nomask($payload, $type = 'text') 
{
	$frameHead = array();
	$frame = '';
	$payloadLength = strlen($payload);

	if ($type == 'text')
		$frameHead[0] = 129;
	else if ($type == 'close')
		$frameHead[0] = 136;
	else if ($type == 'ping')
		$frameHead[0] = 137;
	else if ($type == 'pong')
		$frameHead[0] = 138;

	if ($payloadLength > 65535) 
	{
		$payloadLengthBin = str_split(sprintf('%064b', $payloadLength), 8);
		$frameHead[1] = 127;
		for ($i = 0; $i < 8; $i++)
			$frameHead[$i + 2] = bindec($payloadLengthBin[$i]);

		if ($frameHead[2] > 127) 
		{
			$this->close(1004);
			return false;
		}
	}
	elseif ($payloadLength > 125) 
	{
		$payloadLengthBin = str_split(sprintf('%016b', $payloadLength), 8);
		$frameHead[1] = 126;
		$frameHead[2] = bindec($payloadLengthBin[0]);
		$frameHead[3] = bindec($payloadLengthBin[1]);
	}
	else 
		$frameHead[1] = $payloadLength;

	foreach (array_keys($frameHead) as $i)
		$frameHead[$i] = chr($frameHead[$i]);

	$frame = implode('', $frameHead);
	$frame .= $payload;
	return $frame;
}

function handshake($client)
{
	$headers_stream = fread($client, 1500);
	$headers = array();
	$lines = preg_split("/\r\n/", $headers_stream);
	foreach($lines as $line)
	{
		$line = rtrim($line);
		if(preg_match('/\A(\S+): (.*)\z/', $line, $matches))
			$headers[$matches[1]] = $matches[2];
	}

	$user = array
	(
		"id" => 0,
		"admin" => 0,
		"email" => "unauthenticated user",
		"ip" => $headers['X-Real-IP'], 
		"socket" => $client,
		"port" => explode(':', stream_socket_get_name($client, true))[1],
		"requests" => 0,
		"connection_time" => microtime()
	);

	$token = $headers['Cookie'];
	$token = substr($token, strpos($token, 'token=') + 6);
	if (strpos($token, ';'))
		$token = substr($token, 0, strpos($token, ';'));

	try
	{
		$payload = (new JWT(getenv('API_SHA_KEY'), 'HS512', 3600, 10))->decode($token);
		$user['id'] = $payload['user']->id;
		$user['admin'] = $payload['user']->admin;
		$user['email'] = $payload['user']->email;
		$user['token'] = $token;
		$user['expires'] = $payload['exp'];
	}
	catch (Throwable $e)
	{
		$user['auth_error'] = 'invalid token';
	}

	$ip_and_port = stream_socket_get_name($user['socket'], true);
	$ip = explode(':', $ip_and_port)[0];
	$port = explode(':', $ip_and_port)[1];

	$secKey = $headers['Sec-WebSocket-Key'];
	$secAccept = base64_encode(pack('H*', sha1($secKey . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
	$upgrade  = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
	"Upgrade: websocket\r\n" .
	"Connection: Upgrade\r\n" .
	"WebSocket-Origin: " . $headers['X-Real-IP'] . "\r\n" .
	"WebSocket-Location: wss://" . $headers['X-Real-IP'] . ":" . $port . "\r\n".
	"Sec-WebSocket-Version: 13\r\n" .
	"Sec-WebSocket-Accept:" . $secAccept . "\r\n\r\n";
	fwrite($user['socket'], $upgrade);

	return $user;
}

function disconnect($user, $code, $reason)
{
	global $users;
	//error_log("Websocket connection closed with " . $user['email'] . " at " . $user['ip'] . ":" . $user['port'] . " (" . $reason .")");
	if (is_resource($user['socket']))
	{
		fwrite($user['socket'], nomask(pack('n*', $code) . $reason, $type = 'close', false));
		fclose($user['socket']);
	}
	for($i = 0; $i < sizeof($users); $i++)
		if ($users[$i]['ip'] == $user['ip'] AND $users[$i]['port'] == $user['port'])
			array_splice($users, $i, 1);
}

$context = stream_context_create();
$server = stream_socket_server("tcp://0.0.0.0:20000", $errno, $errstr);
if (!$server)
	error_log("$errstr ($errno)");

$clients = array($server);
$users = array();
$last_second = time();

error_log("Websocket server listening for connections...");

while(true)
{
	$changed = array($server);
	foreach($users as $user)
		array_push($changed, $user['socket']);

	if (stream_select($changed, $null, $null, 0, 10) > 0)
	{
		if (in_array($server, $changed)) 
		{
			$client = @stream_socket_accept($server);
			if (!$client)
				continue;
			
			stream_set_blocking($client, true);
			$user = handshake($client);
			array_push($users, $user);
			stream_set_blocking($client, false);
			
			//error_log("Websocket connection from " . $user['email'] . " at " . $user['ip'] . ":" . $user['port'] . ($user['auth_error'] ? " (" . $user['auth_error'] . ")" : ""));
			if ($user['auth_error'] != "")
				disconnect($user, 1008, $user['auth_error']);

			$found_socket = array_search($server, $changed);
			unset($changed[$found_socket]);
		}

		foreach ($changed as $changed_socket) 
		{
			for ($i=0; $i<sizeof($users); $i++)
				if ($users[$i]['socket'] == $changed_socket)
					$user_index = $i;

			$current_user = &$users[$user_index];

			//FLOOD PROTECTION (MAX RATE)
			$users[$user_index]['requests']++;
			if ($users[$user_index]['requests'] > 100)
			{
				disconnect($users[$user_index], 1008, "Flood protection : Too much requests (" . $users[$user_index]['requests'] . ")");
				continue;
			}

			//PROCESS INCOMING DATA
			if ($current_user['socket'])
				$current_user['stream'] .= fread($current_user['socket'], 1048576);
			else
				disconnect($current_user, 1000, 'Connection lost');

			if (strlen($current_user['stream']) == 0)
				disconnect($current_user, 1007, "Stream with no data");

			//FLOOD PROTECTION (STREAM SIZE)
			if (strlen($current_user['stream']) == 8192)
			{
				disconnect($current_user, 1009, "Flood protection : Too large payload (max 8192 bytes)");
				continue;
			}

			$stream = $current_user['stream'];
			while ($stream !== '')
			{
				$users[$user_index]['requests']++;	
				if ($users[$user_index]['requests'] > 100)
				{
					disconnect($users[$user_index], 1008, "Flood protection : Too much requests (" . $users[$user_index]['requests'] . ")");
					$stream = '';
					continue;
				}

				unset($opcode);
				unset($payload);

				$opcode = @bindec(substr(sprintf('%08b', ord($stream[0])), 4, 4));
				$payload_length  = ord($stream[1]) & 127;

				$mask_offset = 2;
				$payload_offset = 6;
					
				if($payload_length == 126)
				{
					$mask_offset = 4;
					$payload_offset = 8;
					$payload_length = (ord($stream[2]) << 8) + ord($stream[3]);
				}
				else if($payload_length == 127)
				{
					$mask_offset = 10;
					$payload_offset = 14;
					$payload_length  = (ord($stream[2]) << 56) + (ord($stream[3]) << 48) + (ord($stream[4]) << 40) + (ord($stream[5]) << 32) + (ord($stream[6]) << 24) +(ord($stream[7]) << 16) + (ord($stream[8]) << 8) + ord($stream[9]);
				}

				$mask = substr($stream, $mask_offset, 4);
				$encoded_payload = substr($stream, $payload_offset, $payload_length);

				for($i = 0; $i < strlen($encoded_payload); $i++)
					$payload .= $encoded_payload[$i] ^ $mask[$i % 4];

				$stream = substr($stream, $payload_offset + $payload_length);

				//VALID COMMAND RECEIVED
				if ($opcode == 1)
				{
					$input = json_decode($payload);
					if (json_last_error())
					{
						fwrite($current_user['socket'], nomask('{"code":400, "message":"invalid json"}'));
						continue;
					}

					if (!preg_match("/^[a-z0-9-_]+$/", $input->command))
					{
						fwrite($current_user['socket'], nomask('{"code":400, "message":"invalid command"}'));
						continue;
					}

					if ($input->command == 'disconnect')
						disconnect($current_user, 1000, "Graceful disconnection");

					if (!function_exists('optimus_' . $input->command) && file_exists('/srv/websocket/' . $input->command . '.php'))
						include_once('/srv/websocket/' . $input->command . '.php');
					if (!function_exists('optimus_' . $input->command))
					{
						foreach($users as $user)
							if ($input->user == $user["id"] OR $input->user == "all" OR ($input->user == "admin" AND $user['admin'] == 1))
								fwrite($user['socket'], nomask('{"code":200, "command": "' . $input->command . '","data":' . json_encode($input->data) . '}'));
					}
					else
					{
						$command = 'optimus_' . $input->command;
						$command();
					}
				}
				else if ($opcode == 8)
					disconnect($current_user, 1008, "Graceful disconnection");
			}

			$current_user['stream'] = $stream;
		}
	}

	if (time() > $last_second + 1)
	{
		for ($i=0; $i < sizeof($users); $i++)
		{
			//error_log('--> ' . $users[$i]['email'] . ' at ' . $users[$i]['ip'] . ":" . $users[$i]['port'] . ' (' . $users[$i]['requests'] . '%)');
			$users[$i]['requests'] = max($users[$i]['requests'] - 20, 0);
			if (time() > $users[$i]['expires'] - 1)
			{
				fwrite($users[$i]['socket'], nomask('{"code":200, "command": "token_expired"}'));
				disconnect($users[$i], 1008, "Token expired");
			}
		}

		$last_second = time();
	}

	usleep(1000);
}
?>