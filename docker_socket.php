<?php
//EXAMPLE REQUEST : "GET /v1.41/images/json"
function docker_socket_request($request, $body = null)
{
	$fp = fsockopen('unix:///var/run/docker.sock', -1, $errno, $errstr, 30);
	if (!$fp)
		return array("code" => 500, "message" => "Error " . $errno . " : . $errstr");
	
	$out = $request . " HTTP/1.1\r\n";
	$out .= "Host: localhost\r\n";
	$out .= "Content-Type: application/json\r\n";
	$out .= "Connection: Close\r\n";
	if ($body)
	{
		$out .= "Content-Length: " . strlen($body) . "\r\n\r\n";
		$out .= $body;
	}
	$out .= "\r\n";

	fwrite($fp, $out);
	while (!feof($fp))
		$output .= fgets($fp, 128);

	$output = explode("\r\n\r\n", $output);
	$output_header = $output[0];
	$response_code = explode(' ', $output_header)[1];
	$output_body = $output[1];

	if (strpos($output_body, '[') < strpos($output_body, '{'))
	{
		$firstchar = '[';
		$lastchar = ']';
	}
	else
	{
		$firstchar = '{';
		$lastchar = '}';
	}

	$output_body = strstr($output_body, $firstchar);
	$output_body = substr($output_body, 0, strrpos($output_body, $lastchar)+1);

	if (strlen($output_body) > 1)
	{
		$output_body = json_decode($output_body);
		if (json_last_error() !== JSON_ERROR_NONE)
			$result = array("code" => 400, "message" => "JSON invalide", "error" => json_last_error());
	}

	fclose($fp);
	
	return array("code" => (int)$response_code, "header" => $output_header, "body" => $output_body);
}
?>